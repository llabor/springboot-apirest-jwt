package com.techu.security.jwt.controller;


import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import com.techu.security.jwt.components.AuthHandler;
import com.techu.security.jwt.components.JWTBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/apitechu/jwt")
public class HelloWorldController {
    @Autowired
    JWTBuilder jwtBuilder;

    @GetMapping("/tokenget")
    public String tokenget(@RequestParam(value="nombre", defaultValue="Tech U!") String name){
        return jwtBuilder.generateToken(name,"admin");
    }

    @GetMapping(path="/hello",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public String helloWorld(){
        String s = "Hello from JWT demo...";
        return s;
    }
}
